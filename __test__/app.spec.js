const request = require('supertest');
const app = require('../src/app');

// Given - some precondition
// When - some action executed by the function that you’re testing
// Then - the expected outcome
// it('given a date in the past, colorForDueDate() returns red', () => {
//   expect(colorForDueDate('2000-10-20')).toBe('red');
// });

describe('Test the root path', () => {
  test('It should response the GET method', () => {
    return request(app)
      .get('/')
      .then((response) => {
        expect(response.statusCode).toBe(200);
      });
  });
});

// Async, await way - to use this method we have to ise babel-preset-env package
// describe('Test the root path', () => {
//   test('It should response the GET method', async () => {
//     const response = await request(app).get('/');
//     expect(response.statusCode).toBe(200);
//   });
// });

// Supertest Way
// describe('Test the root path', () => {
//   test('It should response the GET method', async () => {
//     return request(app).get('/').expect(200);
//   });
// });

// About the Database connection
// To prevent race condition - beforeAll() and afterAll() are used.
// describe('Test the postJob method', () => {
//   beforeAll(() => {
//     mongoDD.connect();
//   });
//   afterAll((done) => {
//     mongoDD.disconnect(done);
//   });
// });
