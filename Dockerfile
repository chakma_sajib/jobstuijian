FROM node:14

# RUN mkdir -p /app
WORKDIR /usr/src/app

# When using COPY with more than one source file, the destination must be a directory and end with a /
COPY package*.json  /usr/src/app/
RUN npm install
COPY . /usr/src/app/
EXPOSE 3000
CMD [ "npm", "run", "dev" ]

