const app = require('./src/app');

app.listen(8000, () => {
  console.log('Server is listening on port 8000');
});
