function makeHttpError({ statusCode, errorMessage }) {
  return {
    headers: 'application/json',
    statusCode,
    data: JSON.stringify({
      success: false,
      error: errorMessage
    })
  };
}

module.exports = {
  makeHttpError
};
