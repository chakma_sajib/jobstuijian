function sperateHelperRequest(req = {}) {
  return Object.freeze({
    method: req.method,
    path: req.path,
    pathParams: req.params,
    queryParams: req.query,
    body: req.body
  });
}

exports.sperateHelperRequest = sperateHelperRequest;

// module.exports = {
//   sperateHelperRequest
// };

// export default { sperateHelperRequest }; only can be used in ES6
