const mongodb = require('mongodb');

// How to connect MongoDB with nodejs
// step 1. require - mongodb package and we will get the MongoClient object from it
// step 2. create a URL to the MongoDB server
// step 3. use the mongo.connect() method to get the reference to the MongoDB instance client
// step 4. now you can select a database using client.db() method

// Create and get a collection
// you can get a collection by using the db.collection() method . if the collection does not exist yet, it's created
// e.g - const collection = db.collection("dogs")

// Insert data into a collection a Document
// Use insertOne() method to add an object on "dogs" collection
// e.g. -  collection.insertOne({name: "Roger"}, (err, result)=>{})
// use insertMany() method for adding multiple items on collection

// Find all documents
// use the find() method on the collection to get all the documents added to the collection
// e.g - collection.find().toArray((err, items)=>{ console.log(items)})

// Find a specific document
// pass an object to the find() method to filter the collection based on what you need to retrieve
// e.g - collection.find({name:"Tongo"}).toArray((err, items)=>{ console.log(items)})
// collection.findOne({name::"  Togo"}, (err, item) =>{ console.log(item)})

// Update an existing document
// use the updateOne() method to update a document
// e.g. -  collection.updateOne({name:'Togo'}, {'$set':{'name': 'Togo2'}}, (err,item) =>console.log(item)})

// Delete a document
// use the deleteOne() method to delte a document
// e.g- collection.deleteOne({name:'Tongo'}, (err, item) => {console.log(item)})

// closing the connection
// use close() method on the client object
// e.g - client.close()

// we call also use promises or async/await
// example. promise way

// collection.findOne({ name: 'Togo' })
//   .then((item) => {
//     console.log(item);
//   })
//   .catch((err) => {
//     console.error(err);
//   });

// Async/Await way
// const find = async () => {
//   try {
//     const item = await collection.findOne({ name: 'Togo' });
//   } catch (error) {
//     console.log(error);
//   }
// };

async function makeDb() {
  const MongoClient = mongodb.MongoClient;
  const url = process.env.DB_URL;
  const dbName = process.env.DBNAME;
  const client = new MongoClient(url, { useNewUrlParser: true });
  await client.connect();
  const db = await client.db(dbName);
  db.makeId = makeIdFromString;
  return db;
}

function makeIdFromString(id) {
  return new mongodb.ObjectID(id);
}

module.exports = makeDb;
