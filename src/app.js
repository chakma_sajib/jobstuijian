const express = require('express');
const bodyParser = require('body-parser');
const { separateHelperRequest } = require('./helpers');
const { handleJobsRequest } = require('./jobs');

const app = express();
app.use(bodyParser.json());

app.all('/jobs', jobsController);

function jobsController(req, res) {
  const httpRequest = separateHelperRequest(req);

  handleJobsRequest(httpRequest)
    .then(({ headers, statusCode, data }) =>
      res.set(headers).status(statusCode).send(data)
    )
    .catch((error) => console.log(error));
}

app.get('/', (req, res) => {
  console.log(req.body);
  return res.send({ Name: 'Sajib Chakma' });
});

module.exports = app;
