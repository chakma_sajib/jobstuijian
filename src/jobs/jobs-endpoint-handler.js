const { makeHttpError } = require('../helpers/httpError');

function makeJobsEndpointHandler() {
  return async function handle(httpRequest) {
    switch (httpRequest.method) {
      case 'POST':
        return postJobs(httpRequest);
      default:
        return makeHttpError({
          statusCode: 405,
          errorMessage: `${httpRequest.method} method not allowed.`
        });
    }
  };
}

async function postJobs(httpRequest) {
  console.log('postjobs method is calling ');

  let jobPostDetail = httpRequest.body;

  if (!jobPostDetail) {
    return makeHttpError({
      statusCode: 400,
      errorMessage: 'Bad Request. No Post Body'
    });
  }

  if (typeof httpRequest.body === 'string') {
    try {
      jobPostDetail = JSON.parse(jobPostDetail);
    } catch (error) {
      return makeHttpError({
        statusCode: 400,
        errorMessage: 'Bad request. POST body must be valid JSON.'
      });
    }
  }

  try {
    return {
      headers: {
        'Content-Type': 'application/json'
      },
      statusCode: 200,
      data: JSON.stringify(jobPostDetail)
    };
  } catch (error) {
    return makeHttpError({
      statusCode: 409,
      errorMessage: error.message
    });
  }
}

exports.makeJobsEndpointHandler = makeJobsEndpointHandler;
