const { makeJobsEndpointHandler } = require('./jobs-endpoint-handler.js');

const handleJobsRequest = makeJobsEndpointHandler();

module.exports = {
  handleJobsRequest: handleJobsRequest
};
