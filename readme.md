1. Run and build to create image : type - docker build -t image-name

2. Run docker image to make it docker container : docker run -it -p localhost-port:container-port container-id

Explain: Here, -it means - interactively and -p flag means to mapping our entire container port with localhost port 9000 then write down the container image name

3. run docker container on background : docker run -d -p port-number:container-port-number docker-image

Explain: -d runs the container in detached mode then, again -p flag redirects a public port to a private port inside the containe and finally run the docker image

4. docker run -it -p localhost-port:container-port-v \$(pwd):/app or using docker-compose run- docker-compose up or docker-compose down / docker-compose up -d container-name

Note: On going
